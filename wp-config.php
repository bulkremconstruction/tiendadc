<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'estampados' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'torres' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'W[TuTXT1/i:;MA2o-R~]T{M.O9OVC#b$/J2AjC[BKo?if:o(~viw`bzn {;$Pv)C' );
define( 'SECURE_AUTH_KEY',  'Nxku;@,tn7r*9FSoXL::nMRlLeUba5}K_sl=Cv($#%Yp}|CU5Bszx6D@Y,;J_?=@' );
define( 'LOGGED_IN_KEY',    '4 ZngnyqUj}:C<[$S8DEV.J?%mg !RrXmn1YX4=dn2WbujM20 <*AcMx6M,uU@9s' );
define( 'NONCE_KEY',        '{PPQM[JKI7`iTxCVT(R<~uw3x9;;Q3@&Ppja!5f,l0,%x,={e7e&:t*ve~ K%K7C' );
define( 'AUTH_SALT',        '_,k]EVoS&g@Z4*N7|cN9B@W (91jI[<YrB&|F&]2F3!3to5Hwsrf9sF%@}HFE*8H' );
define( 'SECURE_AUTH_SALT', '_Vv/e##zX]GV;)=*114Q<B5EM#TSRZjLoGI 9wh$j5Eh2WUK~v|Z~?4PS41,g@mN' );
define( 'LOGGED_IN_SALT',   '!8YAP,|1@;v;zl>C2yMB+QFG(:Zij#SK](^Yw8*L:cc_~0%dbrBx=<gv[K<q)WGx' );
define( 'NONCE_SALT',       'm.5g+<NVHzew_:J}Vj8mq-2N_^b=]A4L^^|%u,m^hIW;jKGc0oJm!hmB!7Hsz@p,' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
